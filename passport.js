const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const LocalStrategy = require('passport-local').Strategy;
const config = require('./configuration');
const User = require('./src/models/user/user.mdl');




// JSON WEB TOKENS STRATEGY
passport.use(new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: config.JWT_SECRET
}, async (payload, done) => {
  var userModel = new User;
  try {
    // Find the user specified in token
    const user = await userModel.findById(payload.sub);

    // If user doesn't exists, handle it
    if (!user) {
      return done(null, false);
    }

    // Otherwise, return the user
    done(null, user);
  } catch (error) {
    done(error, false);
  }
}));

passport.use(new LocalStrategy({
  usernameField: 'email'
}, async (email, password, done) => {
  var userModel = new User;
  try {
    const user = await userModel.findByAttribute(email);
    if (!user) {
      return done(null, false);
    }
    const isMatch = await userModel.isValidPassword(user.password,password);
    if (!isMatch) {
      return done(null, false);
    }
    done(null, user);
  }
  catch (error) {
    done(error, false);

  }
}));
