const router = require('express-promise-router')();
const {validateBody,schemas} = require('../helpers/routeHelpers');
const UserController = require('../src/controllers/user.cntrl');
const passport = require('passport');   
const passportConf= require('../passport');
const passportJWT = passport.authenticate('jwt', { session: false });
const passportLocal = passport.authenticate('local', { session: false });

var userCntrl = new UserController();

// router.route('/signup')
// .post(validateBody(schemas.authSchema), (req,res) => {
//     userCntrl.signUp(req,res);
// });

router.route('/signup')
.post((req,res) => {
    userCntrl.signUp(req,res);
});


router.route('/signin')
.post(passportLocal,(req,res) => {
    userCntrl.signIn(req,res);
});

module.exports=router;