const router = require('express-promise-router')();

const PurchaseOrderController = require('../src/controllers/purchaseOrder.cntrl');
const passport = require('passport');   
const passportConf= require('../passport');
const passportJWT = passport.authenticate('jwt', { session: false });


var poController = new PurchaseOrderController();

// router.route('/insert')
// .post(passportJWT,(req,res) => {
//     poController.insert(req,res);
// });

router.route('/insert')
.post((req,res) => {
   poController.insert(req,res);
});

router.route('/update')
.post((req,res) => {
   poController.update(req,res);
});

router.route('/getPO')
.get((req,res) => {
   poController.getByID(req,res);
});

router.route('/delete')
.post((req,res) => {
   poController.delete(req,res);
});

router.route('/list')
.get((req,res) => {
   poController.list(req,res);
});

router.route('/refData')
.get(passportJWT,(req,res) => {
   poController.getreferenceModel(req,res);
});

module.exports=router;