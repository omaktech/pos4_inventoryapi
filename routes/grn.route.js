const router = require("express-promise-router")();

const GrnController = require("../src/controllers/grn.cntrl");
const passport = require("passport");
const passportConf = require("../passport");
const passportJWT = passport.authenticate("jwt", { session: false });

var grnController = new GrnController();

router.route("/list").get((req, res) => {
  grnController.list(req, res);
});

router.route("/insert").post((req, res) => {
  grnController.insert(req, res);
});

router.route("/update").post((req, res) => {
  grnController.update(req, res);
});

router.route("/delete").post((req, res) => {
  grnController.delete(req, res);
});

module.exports = router;
