const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const app = express();
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(morgan("dev"));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// Route
app.use("/users", require("./routes/users"));
app.use("/po", require("./routes/purchaseOrder.route"));
app.use("/grn", require("./routes/grn.route"));

// Start the server
const port = process.env.PORT || 4000;
app.listen(port);
console.log(`server is listning at ${port}`);
