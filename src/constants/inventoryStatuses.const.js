
module.exports = {
    ACTIVE: "0",
    PENDING: "1",
    APPROVED: "2",
    PARTIALLY: "3",
    COMPLETE: "4",
    DELETED: "5",
    DISCOUNTINUED: "6"
}