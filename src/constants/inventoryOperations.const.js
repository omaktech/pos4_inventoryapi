module.exports = {
    VIEW: "View",
    CREATE: "Create",
    UPDATE : "Update",
    APPROVE: "Approve",
    DELETE: "Delete"
}