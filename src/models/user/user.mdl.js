const uuid = require('node-uuid-generator');
const bcrypt = require('bcryptjs');
const db = require("../base");

class User {

    async findById(id) {
        try {
            let where = {
                "selector": {
                    "_id": {
                        "$eq": id
                    }
                }
            };
            var user = await db.dbHandle.find(where);
            if (user.docs[0]) {
                return user.docs[0];
            }
            else {
                return new Error("no data found")

            }
        } catch (error) {
            return error;
        }

    };

    async findByAttribute(email) {
        try {
            let where = {
                "selector": {
                    "email": {
                        "$eq": email
                    }
                }
            };
            var user = await db.dbHandle.find(where);
            if (user.docs[0]) {
                return user.docs[0];
            }
            else {
                return new Error("no data found")

            }
        } catch (error) {
            return error;
        }

    };

    async createUser(user) {
        try {
            user._id = 'User_' + uuid.generate();
            var salt = await bcrypt.genSalt(10);
            user.password = await bcrypt.hash(user.password, salt);
            await db.dbHandle.insert(user);
            return user;
        } catch (error) {
            return error;
        }
    };

    async isValidPassword(userPassword, password) {
       return await bcrypt.compare(password, userPassword);
    }
}

module.exports = User;