
var POHeaderEntity = require("../../entities/purchaseOrder/header.entity");
var POHeaderDTO = require("../../dto/purchaseOrder/header.dto");


class PurchaseOrderHeaderDTOMapper {
    constructor() {
    }

    map(dataEntity) {
        if (!dataEntity instanceof POHeaderEntity) {
            throw new Error("The object is NOT a valid PO Header Entity");
        }

        var poHeaderDto = new POHeaderDTO();
        poHeaderDto._id = dataEntity._id;
        poHeaderDto._rev = dataEntity._rev;
        poHeaderDto.type = dataEntity.type;
        poHeaderDto.group_id = dataEntity.group_id;
        poHeaderDto.Restaurent_Id = dataEntity.Restaurent_Id;
        poHeaderDto.PONumber = dataEntity.PONumber;
        poHeaderDto.PO_date = dataEntity.PO_date;
        poHeaderDto.Document_Number = dataEntity.Document_Number;
        poHeaderDto.Supplier_Id = dataEntity.Supplier_Id;
        poHeaderDto.Status = dataEntity.Status;
        poHeaderDto.User_Id = dataEntity.User_Id;
        poHeaderDto.Remarks = dataEntity.Remarks;
        poHeaderDto.PaymentTermID = dataEntity.PaymentTermID;
        poHeaderDto.NetPrice = dataEntity.NetPrice;
        poHeaderDto.TotalPrice = dataEntity.TotalPrice;
        poHeaderDto.FullDiscountRate = dataEntity.FullDiscountRate;
        poHeaderDto.DiscountPrice = dataEntity.DiscountPrice;
        poHeaderDto.LineDiscountPrice = dataEntity.LineDiscountPrice;
        poHeaderDto.VATPrice = dataEntity.VATPrice;
        poHeaderDto.OtherTaxPrice = dataEntity.OtherTaxPrice;
        poHeaderDto.AdvanceAmount = dataEntity.AdvanceAmount;
        poHeaderDto.DiscountAmount = dataEntity.DiscountAmount;
        return poHeaderDto;
    }
}

module.exports = PurchaseOrderHeaderDTOMapper
