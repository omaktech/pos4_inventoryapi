
const POInsertService = require('../services/commandService/purchaseOrder/insert.service');
const PODeleteService = require('../services/commandService/purchaseOrder/delete.service');
const POGetByIDService = require('../services/queryService/purchaseOrder/getbyID.service');
const POFullUpdateService = require('../services/commandService/purchaseOrder/update.service');
const POGetAllService = require('../services/queryService/purchaseOrder/getAll.service');
const POGetReferenceModelService = require('../services/queryService/purchaseOrder/getReferenceData.service');
const poRepo = require('../repos/couchDBRepos/purchaseOrder.repo');
const poDTOMapper = require('../dtoMappers/purchaseOrder/headerDTO.mapper');
const poDBObjectMapper = require('../entityMapper/purchaseOrder/dbEntityObject.mapper');

const repository = new poRepo();
const dTOMapper = new poDTOMapper();
const objectEntityMapper = new poDBObjectMapper();


class PurchaseOrderController {

    async insert(req, res) {
        try {
            var poService = new POInsertService(req, repository, dTOMapper, objectEntityMapper);
            var purchaseOrder = await poService.execute();
            return res.status(200).json({ purchaseOrder });

        } catch (error) {
            return res.status(500).json({ error });
        }

    };

    async update(req, res) {
        try {
            var poService = new POFullUpdateService(req, repository, dTOMapper, objectEntityMapper);
            var purchaseOrder = await poService.execute();
            return res.status(200).json({ purchaseOrder });

        } catch (error) {
            return res.status(500).json({ error });
        }

    };

    async getByID(req, res) {
        try {
            var poService = new POGetByIDService(req, repository);
            var purchaseOrder = await poService.execute();
            return res.status(200).json({ purchaseOrder });

        } catch (error) {
            return res.status(500).json({ error });
        }

    };

    async list(req, res) {
        try {
            var poService = new POGetAllService(req, repository);
            var purchaseOrders = await poService.execute();
            return res.status(200).json({ purchaseOrders });

        } catch (error) {
            return res.status(500).json({ error });
        }

    };

    async delete(req, res) {
        try {
            var poService = new PODeleteService(req, repository, dTOMapper, objectEntityMapper);
            await poService.execute();
            return res.status(200).json("Purchase Order removed");

        } catch (error) {
            return res.status(500).json({ error });
        }
    };

    async getreferenceModel(req, res) {
        try {
            var poService = new POGetReferenceModelService(req, repository, dTOMapper, objectEntityMapper);
            var refrenceModel = await poService.execute();
            return res.status(200).json(refrenceModel);

        } catch (error) {
            return res.status(500).json({ error });
        }
    };

}


module.exports = PurchaseOrderController;
