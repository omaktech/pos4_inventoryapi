const GrnGetAllService = require("../services/queryService/grn/getAll.service");
const GrnFullUpdateService = require("../services/commandService/grn/update.service");
const GrnDeleteService = require("../services/commandService/grn/delete.service");
const grnRepo = require("../repos/couchDBRepos/grn.repo");

const repository = new grnRepo();

class GrnController {
  async list(req, res) {
    try {
      var grnService = new GrnGetAllService(req, repository);
      var grn = await grnService.execute();
      return res.status(200).json({ grn });
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  async insert(req, res) {
    try {
      var grnService = new GrnInsertService(
        req,
        repository,
        dTOMapper,
        objectEntityMapper
      );
      var grn = await grnService.execute();
      return res.status(200).json({ grn });
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  async update(req, res) {
    try {
      var grnService = new GrnFullUpdateService(
        req,
        repository,
        dTOMapper,
        objectEntityMapper
      );
      var grn = await grnService.execute();
      return res.status(200).json({ grn });
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  async delete(req, res) {
    try {
      var grnService = new GrnDeleteService(
        req,
        repository,
        dTOMapper,
        objectEntityMapper
      );
      await grnService.execute();
      return res.status(200).json("GRN removed");
    } catch (error) {
      return res.status(500).json({ error });
    }
  }
}

module.exports = GrnController;
