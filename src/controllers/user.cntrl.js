const JWT = require('jsonwebtoken');
const { JWT_SECRET } = require('../../configuration');
const User = require("../models/user/user.mdl");



class UserController {

    signToken(user) {
        return JWT.sign({
            iss: 'CodeWorkr',
            sub: user._id,
            iat: new Date().getTime(), // current time
            exp: new Date().setDate(new Date().getDate() + 1) // current time + 1 day ahead
        }, JWT_SECRET);
    };

    async signUp(req, res) {
        try {

            var userModel = new User();

            const { email, password } = req.body;
            var newUser = await userModel.createUser({ email, password })
            var userModel = {
                _id: newUser._id,
                email: newUser.email,
                password: newUser.password
            };
            //Responde with Token
            const token = this.signToken(userModel);
            res.status(200).json({ token });
        } catch (error) {
            res.status(500).json({ error });
        }

    };

    async signIn(req, res) {
        try {
            var user = {
                _id: req.user._id,
                email: req.user.email,
                password: req.user.password
            }
            const token = this.signToken(user);
            return res.status(200).json({ token });
        } catch (error) {
            return res.status(500).json({ error });
        }
    };
}


module.exports = UserController;
