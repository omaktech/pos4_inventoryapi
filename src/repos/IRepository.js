class IRepository {
    constructor() {
        if (this.add === undefined) {
            throw new Error("Must override .add() method");
        }

        if (this.update === undefined) {
            throw new Error("Must override .update() method");
        }

        if (this.getByUid=== undefined) {
            throw new Error("Must override .getByUid() method");
        }

        // if (this.get === undefined) {
        //     throw new Error("Must override .get() method");
        // }

        // if (this.remove === undefined) {
        //     throw new Error("Must override .remove() method");
        // }
    }

}

module.exports = IRepository;
