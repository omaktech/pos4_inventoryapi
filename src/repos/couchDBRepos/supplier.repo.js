const db = require("../../models/base");

class SupplierRepo {
  async getAll() {
    try {
      const q = {
        selector: {
          type: {
            $eq: "supplier"
          }
        }
      };
      return await db.dbHandle.find(q);
    } catch (error) {
      return error;
    }
  }

  // async getByUid(id) {
  //   try {
  //     var supplier = await db.dbHandle.get(id);
  //     return supplier;
  //   } catch (error) {
  //     return error;
  //   }
  // }
}

module.exports = SupplierRepo;
