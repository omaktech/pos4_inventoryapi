const db = require("../../models/base");

class SubCategoryRepo {
  async getAll() {
    try {
      const q = {
        selector: {
          type: {
            $eq: "subcategory"
          }
        }
      };
      return await db.dbHandle.find(q);
    } catch (error) {
      return error;
    }
  }
}

module.exports = SubCategoryRepo;
