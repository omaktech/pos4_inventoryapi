const db = require("../../models/base");

class ItemRepo {
  async getAll() {
    try {
      const q = {
        selector: {
          type: {
            $eq: "item"
          }
        }
      };
      return await db.dbHandle.find(q);
    } catch (error) {
      return error;
    }
  }
}

module.exports = ItemRepo;
