
const db = require("../../models/base");

class RestaurentRepo {

    async getAll() {
        try {
            const q = {
                "selector": {
                   "type": {
                      "$eq": "OUTLET"
                   }
                }
             };
             return await db.dbHandle.find(q);
        } catch (error) {
            return error;
        }
    };
}

module.exports = RestaurentRepo;
