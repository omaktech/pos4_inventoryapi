const IRepository = require("../IRepository");
const db = require("../../models/base");

class GrnRepo extends IRepository {
  async getAll() {
    try {
      const q = {
        selector: {
          type: {
            $eq: "GRN"
          }
        }
      };
      var grn = await db.dbHandle.find(q);
      return grn;
    } catch (error) {
      return error;
    }
  }

  async getByUid(id) {
    try {
      var grn = await db.dbHandle.get(id);
      return grn;
    } catch (error) {
      return error;
    }
  }

  async add(grn) {
    try {
      var newGrn = await db.dbHandle.insert(grn);
      return newGrn;
    } catch (error) {
      return error;
    }
  }

  async update(grn) {
    try {
      var updatedGrn = await db.dbHandle.insert(grn);
      return updatedGrn;
    } catch (error) {
      return error;
    }
  }

  // removing document completely from DB
  async remove(grn) {
    try {
      await db.dbHandle.destroy(grn._id, grn._rev);
      return grn;
    } catch (error) {
      return error;
    }
  }
}

module.exports = GrnRepo;
