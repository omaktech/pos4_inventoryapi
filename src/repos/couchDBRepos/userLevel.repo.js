
const db = require("../../models/base");

class UserLevelRepo {

    async getByUid(id) {
        try {
            return await db.dbHandle.get(id);
        } catch (error) {
            return error;
        }
    };
}

module.exports = UserLevelRepo;
