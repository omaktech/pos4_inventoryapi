
const IRepository = require("../IRepository");
const db = require("../../models/base");

class PurchaseOrderRepo extends IRepository {

    async add(purchaseOrder) {
        try {
           var newPO =  await db.dbHandle.insert(purchaseOrder);
            return newPO;
        } catch (error) {
            return error;
        }
    };

    async getByUid(id) {
        try {
           var purchaseOrder =  await db.dbHandle.get(id);
            return purchaseOrder;
        } catch (error) {
            return error;
        }
    };

    async getAll() {
        try {
            const q = {
                "selector": {
                   "type": {
                      "$eq": "PO"
                   },
                   "Status": {
                      "$ne": "5"
                   }
                }
             };
           var purchaseOrders =  await db.dbHandle.find(q);
            return purchaseOrders;
        } catch (error) {
            return error;
        }
    };

    async update(purchaseOrder) {
        try {
           var updatedPO =  await db.dbHandle.insert(purchaseOrder);
            return updatedPO;
        } catch (error) {
            return error;
        }
    };

    // removing document completely from DB
    async remove(purchaseOrder){
        try {
            await db.dbHandle.destroy(purchaseOrder._id, purchaseOrder._rev);
            return purchaseOrder;
        } catch (error) {
            return error;
        }
    };
}

module.exports = PurchaseOrderRepo;
