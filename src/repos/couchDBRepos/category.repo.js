const db = require("../../models/base");

class CategoryRepo {

    async getAll() {
        try {
            const q = {
                "selector": {
                   "type": {
                      "$eq": "category"
                   }
                }
             };
             return await db.dbHandle.find(q);
        } catch (error) {
            return error;
        }
    };
}

module.exports = CategoryRepo;