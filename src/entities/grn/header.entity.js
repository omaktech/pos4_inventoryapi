const Decimal = require("decimal");

class GrnHeaderEntity {
  constructor() {
    (this._id = ""),
      (this._rev = ""),
      (this.type = "GRN"),
      (this.group_id = ""),
      (this.Restaurent_Id = ""),
      (this.GrnNumber = ""),
      (this.InvoiceNumber = ""),
      (this.Grn_date = ""),
      (this.Document_Number = ""),
      (this.Supplier_Id = ""),
      (this.Status = ""),
      (this.User_Id = ""),
      (this.Remarks = ""),
      (this.PaymentTermID = ""),
      (this.NetPrice = new Decimal(0)),
      (this.TotalPrice = new Decimal(0)),
      (this.DiscountPercentage = new Decimal(0)),
      (this.FullDiscountRate = new Decimal(0)),
      (this.DiscountPrice = new Decimal(0)),
      (this.LineDiscountPrice = new Decimal(0)),
      (this.TaxPrice = new Decimal(0)),
      (this.DiscountAmount = new Decimal(0)),
      (this.RoundingDiscount = new Decimal(0)),
      (this.OtherCharges = new Decimal(0)),
      (this.FinalBillTotal = new Decimal(0)),
      (this.Items = []);
  }
}

module.exports = GrnHeaderEntity;
