
const Decimal = require('decimal');

class BaseTaxEntity {
  constructor() {
    this.Line_id = "",
      this.Tax_id = "",
      this.Amount = new Decimal(0),

      this.Tax_Name= ""
  }
}

module.exports = BaseTaxEntity
