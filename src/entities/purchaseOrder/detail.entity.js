
const Decimal = require('decimal');

class PurchaseOrderDetailEntity {
    constructor() {
        this.Line_id = "",
        this.Item_Id = "",
        this.UnitOfMeasureID = "",
        this.Remark = "",
        this.Status = "",
        this.TaxCombinationID = "",
        this.Quantity = new Decimal(0),
        this.PendingQuantity = new Decimal(0),
        this.unit_price = new Decimal(0),
        this.NetPrice = new Decimal(0),
        this.LineDiscount = new Decimal(0),
        this.DiscountedPrice = new Decimal(0),
        this.VATPrice = new Decimal(0),
        this.OtherTaxPrice = new Decimal(0),
        this.TotalPrice = new Decimal(0),
        this.Taxes = []
    }
}

module.exports = PurchaseOrderDetailEntity
