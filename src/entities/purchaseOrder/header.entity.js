
const Decimal = require('decimal');

class PurchaseOrderHeaderEntity {
  constructor() {
    this._id = "",
      this._rev = "",
      this.type = "PO",
      this.group_id = "",
      this.Restaurent_Id = "",
      this.PONumber = "",
      this.PO_date = "",
      this.Document_Number = "",
      this.Supplier_Id = "",
      this.Status = "",
      this.User_Id = "",
      this.Remarks = "",
      this.PaymentTermID = "",
      this.NetPrice = new Decimal(0),
      this.TotalPrice = new Decimal(0),
      this.FullDiscountRate = new Decimal(0),
      this.DiscountPrice = new Decimal(0),
      this.LineDiscountPrice = new Decimal(0),
      this.VATPrice = new Decimal(0),
      this.OtherTaxPrice = new Decimal(0),
      this.AdvanceAmount = new Decimal(0),
      this.DiscountAmount = new Decimal(0),
      this.Items = []
   
  }
}

module.exports = PurchaseOrderHeaderEntity
