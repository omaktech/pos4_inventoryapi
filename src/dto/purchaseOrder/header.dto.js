
const Decimal = require('decimal');

class PurchaseOrderHeaderDTO {
  constructor() {
    this._id = "",
      this._rev = "",
      this.type = "PO",
      this.group_id = "",
      this.Restaurent_Id = "",
      this.PONumber = "",
      this.PO_date = "",
      this.Document_Number = "",
      this.Supplier_Id = "",
      this.Status = "",
      this.User_Id = "",
      this.Remarks = "",
      this.PaymentTermID = "",
      this.NetPrice = new Decimal(0),
      this.TotalPrice = new Decimal(0),
      this.FullDiscountRate = new Decimal(0),
      this.DiscountPrice = new Decimal(0),
      this.LineDiscountPrice = new Decimal(0),
      this.VATPrice = new Decimal(0),
      this.OtherTaxPrice = new Decimal(0),
      this.AdvanceAmount = new Decimal(0),
      this.DiscountAmount = new Decimal(0),
      this.Items = [],
      // Properties that differes DTO from its Entity
      this.Restaurent_Name = "",
      this.Supplier_Name = "",
      this.PaymentTerm_Name = "",
      this.StatusString = "",
      this.User_Name = ""
  }
}

module.exports = PurchaseOrderHeaderDTO
