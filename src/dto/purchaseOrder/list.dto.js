class PurchaseOrderListDTO {
  constructor() {
    this.PoNumber = "";
    this.Supplier = "";
    this.OriginalLocation = "";
    this.DestinationLocation = "";
    this.PoDate = "";
    this.Status = "";
    this.PoObject = null;
  }
}

module.exports = PurchaseOrderListDTO;
