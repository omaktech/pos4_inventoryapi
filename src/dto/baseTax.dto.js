
const Decimal = require('decimal');

class BaseTaxDTO {
  constructor() {
    this.Line_id = "",
      this.Tax_id = "",
      this.Amount = new Decimal(0),

      this.Tax_Name= ""
  }
}

module.exports = BaseTaxDTO
