class GrnListDTO {
  constructor() {
    this.GrnNumber = "";
    this.InvoiceNo = "";
    this.Supplier = "";
    this.Location = "";
    this.GrnDate = "";
    this.Status = "";
  }
}

module.exports = GrnListDTO;
