var BaseTaxDto = require("../baseTax.dto");

class GrnTaxDTO extends BaseTaxDto {
  constructor() {
    super();
  }
}

module.exports = GrnTaxDTO;
