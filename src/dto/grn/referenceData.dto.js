class GrnReferenceDataDTO {
  constructor() {
    (this.IsViewEnabled = false),
      (this.IsCreateEnabled = false),
      (this.IsUpdateEnabled = false),
      (this.IsDeleteEnabled = false),
      (this.IsApprovalEnabled = false),
      (this.Restaurents = []),
      (this.Categories = []),
      (this.SubCategories = []);
  }
}

module.exports = GrnReferenceDataDTO;
