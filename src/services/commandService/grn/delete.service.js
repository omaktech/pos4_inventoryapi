// Update the PODocument status to DELETED

class GrnDeleteService {
  constructor(req, repository, dTOMapper, objectEntityMapper) {
    // get the DTO from the request body
    this.repository = repository;
    this.dTOMapper = dTOMapper;
    this.dbObjectMapper = objectEntityMapper;
    this.dto = req.body;
  }

  async execute() {
    try {
      this.IsUserPrivileged();
      var grnDocument = this.dbObjectMapper.mapForDelete(this.dto);
      return await this.repository.update(grnDocument);
    } catch (err) {
      throw new Error(err);
    }
  }

  IsUserPrivileged(user) {
    // check with DB user level
    return true;
  }
}

module.exports = GrnDeleteService;
