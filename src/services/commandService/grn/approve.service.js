// Full GRN document Approval
class GrnApproveService {
  constructor(req, repository, dTOMapper, objectEntityMapper) {
    // get the DTO from the request body
    this.repository = repository;
    this.dTOMapper = dTOMapper;
    this.dbObjectMapper = objectEntityMapper;
    this.dto = req.body;
  }

  async execute() {
    try {
      this.IsUserPrivileged();
      return await this.Save();
    } catch (err) {
      throw new Error(err);
    }
  }

  IsUserPrivileged(user) {
    // check with DB user level
    return true;
  }

  async Save() {
    try {
      var grnDocument = this.dbObjectMapper.mapForFinalApproval(this.dto);
      if (grnDocument != undefined)
        return await this.repository.update(grnDocument);
      // must re converted to DTO and then only return should happen
    } catch (error) {
      throw new Error(error);
    }
  }
}

module.exports = GrnApproveService;
