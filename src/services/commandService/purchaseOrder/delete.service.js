
// Update the PODocument status to DELETED

class PurchaseOrderDeleteService {
    constructor(req, repository, dTOMapper, objectEntityMapper) {
        // get the DTO from the request body
        this.repository = repository;
        this.dTOMapper = dTOMapper;
        this.dbObjectMapper = objectEntityMapper;
        this.dto = req.body;
    }

    async execute() {
        try {

            this.IsUserPrivileged();
            var poDocument = this.dbObjectMapper.mapForDelete(this.dto);
            return await this.repository.update(poDocument);
        }
        catch (err) {
            throw new Error(err);
        }
    }

    IsUserPrivileged(user) {
        // check with DB user level 
        return true;
    }

  
}

module.exports = PurchaseOrderDeleteService;