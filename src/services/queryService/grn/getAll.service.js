const listDTO = require("../../../dto/grn/list.dto");

class GrnGetAllService {
  constructor(req, repository) {
    // get the DTO from the request body
    this.repository = repository;
  }

  async execute() {
    try {
      var finalGrnList = [];
      var grnList = await this.repository.getAll();
      for (var i = 0; i < grnList.docs.length; i++) {
        var grnListDto = new listDTO();
        grnListDto.GrnNumber = grnList.docs[i].GrnNumber;
        grnListDto.InvoiceNo = grnList.docs[i].InvoiceNo;
        grnListDto.Supplier = grnList.docs[i].Restaurent_Id;
        grnListDto.Location = grnList.docs[i].Restaurent_Id;
        grnListDto.GrnDate = grnList.docs[i].PO_date;
        grnListDto.Status = grnList.docs[i].Status;
        finalGrnList.push(grnListDto);
      }
      return finalGrnList;
    } catch (err) {
      throw new Error(err);
    }
  }
}

module.exports = GrnGetAllService;
