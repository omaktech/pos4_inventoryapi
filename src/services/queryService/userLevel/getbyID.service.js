
class UserLevelGetByIDService {
    constructor(req, repository) {
        // get the DTO from the request body
        this.repository = repository;
    }

    async getByID(id) {
        try {
            return await this.repository.getByUid(id);
        }
        catch (err) {
            throw new Error(err);
        }
    }

}

module.exports = UserLevelGetByIDService;