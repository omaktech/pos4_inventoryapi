class ItemGetAllService {
  constructor(req, repository) {
    // get the DTO from the request body
    this.repository = repository;
  }

  async execute() {
    try {
      return await this.repository.getAll();
    } catch (err) {
      throw new Error(err);
    }
  }
}

module.exports = ItemGetAllService;
