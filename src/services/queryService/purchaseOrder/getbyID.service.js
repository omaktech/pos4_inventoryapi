
class PurchaseOrderGetByIDService {
    constructor(req, repository) {
        // get the DTO from the request body
        this.repository = repository;
        this._id = req.headers._id;
    }

    async execute() {
        try {
            if (this._id != undefined)
                return await this.repository.getByUid(this._id);
        }
        catch (err) {
            throw new Error(err);
        }
    }

}

module.exports = PurchaseOrderGetByIDService;