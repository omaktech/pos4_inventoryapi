const listDTO = require("../../../dto/purchaseOrder/list.dto");

class PurchaseOrderGetAllService {
  constructor(req, repository) {
    // get the DTO from the request body
    this.repository = repository;
  }

  async execute() {
    try {
      var finalPoList = [];
      var poList = await this.repository.getAll();
      for (var i = 0; i < poList.docs.length; i++) {
        var poListDto = new listDTO();
        poListDto.PoNumber = poList.docs[i].PONumber;
        poListDto.Supplier = poList.docs[i].Supplier_Name;
        poListDto.OriginalLocation = poList.docs[i].Restaurent_Name;
        poListDto.DestinationLocation = poList.docs[i].Restaurent_Name;
        poListDto.PoDate = poList.docs[i].PO_date;
        poListDto.Status = poList.docs[i].Status;
        poListDto.PoObject = poList.docs[i];
        finalPoList.push(poListDto);
      }
      return finalPoList;
    } catch (err) {
      throw new Error(err);
    }
  }
}

module.exports = PurchaseOrderGetAllService;
