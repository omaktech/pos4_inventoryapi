const poRefDatDto = require("../../../dto/purchaseOrder/referenceData.dto");
const userLevelService = require("../userLevel/getbyID.service");
const uselLevelRepo = require("../../../repos/couchDBRepos/userLevel.repo");
const restaurentRepo = require("../../../repos/couchDBRepos/restaurent.repo");
const categoryRepo = require("../../../repos/couchDBRepos/category.repo");
const subCategoryRepo = require("../../../repos/couchDBRepos/subCategory.repo");
const itemRepo = require("../../../repos/couchDBRepos/item.repo");
const supplierRepo = require("../../../repos/couchDBRepos/supplier.repo");
const restaurentService = require("../restaurent/getAll.service");
const categoryService = require("../category/getAll.service");
const subCategoryService = require("../subCategory/getAll.service");
const itemService = require("../item/getAll.service");
const supplierService = require("../supplier/getAll.service");
const moduleConst = require("../../../constants/mainModules.const");
const componentConst = require("../../../constants/inventoryComponents.const");
const operationConst = require("../../../constants/inventoryOperations.const");

class PurchaseOrderGetReferenceDataService {
  constructor(req, repository) {
    // get the DTO from the request body
    this.repository = repository;
    this.user = req.user;
    this.porefData = new poRefDatDto();
  }

  async execute() {
    try {
      // Get LoggedUser data
      await this.FillLoggedUserPrivileges();

      // Get Retaurents
      await this.FillRestaurents();

      // Get Item Categories
      await this.FillCategories();

      // Get Item Sub Categories
      await this.FillSubCategories();

      // Get Items
      await this.FillItems();

      // Get Suppliers
      await this.FillSuppliers();

      //return model
      return this.porefData;
    } catch (err) {
      throw new Error(err);
    }
  }

  async FillLoggedUserPrivileges() {
    try {
      var userRepo = new uselLevelRepo();
      var service = new userLevelService(null, userRepo);
      var userLevel = await service.getByID(this.user.user_level_id);

      if (userLevel != undefined) {
        var invModule = userLevel.modules.find(
          obj => obj.module_name == moduleConst.INV
        );

        if (invModule == undefined || invModule.action == "disable") {
          throw new Error(
            moduleConst.INV + " module not allowed..Cannot Proceed"
          );
        } else {
          var poComopnent = invModule.components.find(
            obj => obj.component_name == componentConst.PO
          );
          if (poComopnent == undefined || poComopnent.action == "disable") {
            throw new Error(componentConst.PO + " not allowed..Cannot Proceed");
          } else {
            var viewOperation = poComopnent.operations.find(
              obj => obj.operation_name == operationConst.VIEW
            );
            this.porefData.IsViewEnabled =
              viewOperation == undefined || viewOperation.action == "disable"
                ? false
                : true;

            var createOperation = poComopnent.operations.find(
              obj => obj.operation_name == operationConst.CREATE
            );
            this.porefData.IsCreateEnabled =
              createOperation == undefined ||
              createOperation.action == "disable"
                ? false
                : true;

            var updateOperation = poComopnent.operations.find(
              obj => obj.operation_name == operationConst.UPDATE
            );
            this.porefData.IsUpdateEnabled =
              updateOperation == undefined ||
              updateOperation.action == "disable"
                ? false
                : true;

            var deleteOperation = poComopnent.operations.find(
              obj => obj.operation_name == operationConst.DELETE
            );
            this.porefData.IsDeleteEnabled =
              deleteOperation == undefined ||
              deleteOperation.action == "disable"
                ? false
                : true;

            var approveOperation = poComopnent.operations.find(
              obj => obj.operation_name == operationConst.APPROVE
            );
            this.porefData.IsApprovalEnabled =
              approveOperation == undefined ||
              approveOperation.action == "disable"
                ? false
                : true;
          }
        }
      } else {
        throw new Error("User Level not found..Cannot Proceed");
      }
    } catch (error) {
      throw new Error(error);
    }
  }

  async FillRestaurents() {
    var resRepo = new restaurentRepo();
    var service = new restaurentService(null, resRepo);
    var list = await service.execute();
    for (var i = 0; i < list.docs.length; i++) {
      var resDTO = {
        Res_Id: list.docs[i]._id,
        Res_Name: list.docs[i].name
      };

      this.porefData.Restaurents.push(resDTO);
    }
  }

  async FillCategories() {
    var catRepo = new categoryRepo();
    var service = new categoryService(null, catRepo);
    var list = await service.execute();
    for (var i = 0; i < list.docs[0].categories.length; i++) {
      var categoryDTO = {
        Category_Id: list.docs[0].categories[i].category_id,
        Category_Name: list.docs[0].categories[i].name
      };
      this.porefData.Categories.push(categoryDTO);
    }
  }

  async FillSubCategories() {
    var subCatRepo = new subCategoryRepo();
    var service = new subCategoryService(null, subCatRepo);
    var list = await service.execute();
    for (var i = 0; i < list.docs[0].subcategories.length; i++) {
      var subCategoryDTO = {
        SubCategory_Id: list.docs[0].subcategories[i].subcategory_id,
        SubCategory_Name: list.docs[0].subcategories[i].name,
        Category_Id: list.docs[0].subcategories[i].category_id
      };
      this.porefData.SubCategories.push(subCategoryDTO);
    }
  }

  async FillItems() {
    var repo = new itemRepo();
    var service = new itemService(null, repo);
    var list = await service.execute();
    for (var i = 0; i < list.docs.length; i++) {
      var itemDTO = {
        Item_Id: list.docs[i]._id,
        Item_Name: list.docs[i].name,
        Subcategory_id: list.docs[i].subcategory_id,
        Item_code: list.docs[i].item_code,
        Price: list.docs[i].price
      };

      this.porefData.Items.push(itemDTO);
    }
  }

  async FillSuppliers() {
    var repo = new supplierRepo();
    var service = new supplierService(null, repo);
    var list = await service.execute();
    for (var i = 0; i < list.docs.length; i++) {
      var supplierDTO = {
        Supplier_Id: list.docs[i]._id,
        Supplier_Name: list.docs[i].name
      };

      this.porefData.Suppliers.push(supplierDTO);
    }
  }
}

module.exports = PurchaseOrderGetReferenceDataService;
