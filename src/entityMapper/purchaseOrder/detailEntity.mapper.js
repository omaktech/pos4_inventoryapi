
const PODetailEntity = require("../../entities/purchaseOrder/detail.entity");
const POTaxEntityMapper = require("../../entityMapper/purchaseOrder/taxEntity.mapper");
const UIDGenerator = require("../../../helpers/uniqueIDGenerator.hlpr");
const StatusConstants = require("../../constants/inventoryStatuses.const");

class PurchaseOrderDetailEntityMapper {
    constructor() {
    }

    map(dataDTO) {

        var poDetailEntity = new PODetailEntity();
        if (dataDTO.Line_id == "") {
            dataDTO.Line_id = UIDGenerator.create();
        }
        poDetailEntity.Line_id = dataDTO.Line_id;
        poDetailEntity.Item_Id = dataDTO.Item_Id;
        poDetailEntity.UnitOfMeasureID = dataDTO.UnitOfMeasureID;
        poDetailEntity.Remark = dataDTO.Remark;
        poDetailEntity.Status = dataDTO.Status;
        poDetailEntity.TaxCombinationID = dataDTO.TaxCombinationID;
        poDetailEntity.Quantity = dataDTO.Quantity;
        poDetailEntity.PendingQuantity = dataDTO.PendingQuantity;
        poDetailEntity.unit_price = dataDTO.unit_price;
        poDetailEntity.NetPrice = dataDTO.NetPrice;
        poDetailEntity.LineDiscount = dataDTO.LineDiscount;
        poDetailEntity.DiscountedPrice = dataDTO.DiscountedPrice;
        poDetailEntity.VATPrice = dataDTO.VATPrice;
        poDetailEntity.OtherTaxPrice = dataDTO.OtherTaxPrice;
        poDetailEntity.TotalPrice = dataDTO.TotalPrice;

        if (dataDTO.Taxes != undefined) {
            var taxEntityMapper = new POTaxEntityMapper();
            dataDTO.Taxes.forEach(function (item) {
                var poTaxItemEntity = new taxEntityMapper.map(item);
                poDetailEntity.Taxes.add(poTaxItemEntity);
            });
        }
        return poDetailEntity;
    };

    mapForDelete(dataDTO) {

        var poDetailEntity = new PODetailEntity();
        poDetailEntity.Line_id = dataDTO.Line_id;
        poDetailEntity.Item_Id = dataDTO.Item_Id;
        poDetailEntity.UnitOfMeasureID = dataDTO.UnitOfMeasureID;
        poDetailEntity.Remark = dataDTO.Remark;
        poDetailEntity.Status = StatusConstants.DELETED;
        poDetailEntity.TaxCombinationID = dataDTO.TaxCombinationID;
        poDetailEntity.Quantity = dataDTO.Quantity;
        poDetailEntity.PendingQuantity = dataDTO.PendingQuantity;
        poDetailEntity.unit_price = dataDTO.unit_price;
        poDetailEntity.NetPrice = dataDTO.NetPrice;
        poDetailEntity.LineDiscount = dataDTO.LineDiscount;
        poDetailEntity.DiscountedPrice = dataDTO.DiscountedPrice;
        poDetailEntity.VATPrice = dataDTO.VATPrice;
        poDetailEntity.OtherTaxPrice = dataDTO.OtherTaxPrice;
        poDetailEntity.TotalPrice = dataDTO.TotalPrice;

        if (dataDTO.Taxes != undefined) {
            var taxEntityMapper = new POTaxEntityMapper();
            dataDTO.Taxes.forEach(function (item) {
                var poTaxItemEntity = new taxEntityMapper.map(item);
                poDetailEntity.Taxes.add(poTaxItemEntity);
            });
        }
        return poDetailEntity;
    };

    mapForFinalApproval(dataDTO) {

        var poDetailEntity = new PODetailEntity();
        poDetailEntity.Line_id = dataDTO.Line_id;
        poDetailEntity.Item_Id = dataDTO.Item_Id;
        poDetailEntity.UnitOfMeasureID = dataDTO.UnitOfMeasureID;
        poDetailEntity.Remark = dataDTO.Remark;
        poDetailEntity.Status = StatusConstants.APPROVED;
        poDetailEntity.TaxCombinationID = dataDTO.TaxCombinationID;
        poDetailEntity.Quantity = dataDTO.Quantity;
        poDetailEntity.PendingQuantity = dataDTO.PendingQuantity;
        poDetailEntity.unit_price = dataDTO.unit_price;
        poDetailEntity.NetPrice = dataDTO.NetPrice;
        poDetailEntity.LineDiscount = dataDTO.LineDiscount;
        poDetailEntity.DiscountedPrice = dataDTO.DiscountedPrice;
        poDetailEntity.VATPrice = dataDTO.VATPrice;
        poDetailEntity.OtherTaxPrice = dataDTO.OtherTaxPrice;
        poDetailEntity.TotalPrice = dataDTO.TotalPrice;

        if (dataDTO.Taxes != undefined) {
            var taxEntityMapper = new POTaxEntityMapper();
            dataDTO.Taxes.forEach(function (item) {
                var poTaxItemEntity = new taxEntityMapper.map(item);
                poDetailEntity.Taxes.add(poTaxItemEntity);
            });
        }
        return poDetailEntity;
    };
}

module.exports = PurchaseOrderDetailEntityMapper
