

var POHeaderEntityMapper = require("./headerEntity.mapper");

class PurchaseOrderDBObjectMapper {
    constructor() {
    }

    map(dataDTO) {
        var headerMapper = new POHeaderEntityMapper();
        var poJsonObject = headerMapper.map(dataDTO);
       
        return poJsonObject;
    };

    mapForDelete(dataDTO) {
        var headerMapper = new POHeaderEntityMapper();
        var poJsonObject = headerMapper.mapForDelete(dataDTO);
       
        return poJsonObject;
    };

    mapForFinalApproval(dataDTO) {
        var headerMapper = new POHeaderEntityMapper();
        var poJsonObject = headerMapper.mapForFinalApproval(dataDTO);
       
        return poJsonObject;
    };
}

module.exports = PurchaseOrderDBObjectMapper
