
const POHeaderEntity = require("../../entities/purchaseOrder/header.entity");
const PODetaiEntityMapper = require("../../entityMapper/purchaseOrder/detailEntity.mapper");
const StatusConstants = require("../../constants/inventoryStatuses.const");

const uuid = require('node-uuid-generator');

class PurchaseOrderHeaderEntityMapper {
    constructor() {
    }

    map(dataEntity) {
        try {
            var poHeaderEntity = new POHeaderEntity();
            if (dataEntity._id == "" ||dataEntity._id === undefined ) {
                dataEntity._id = "PO_"  + uuid.generate();
            }
            poHeaderEntity._id = dataEntity._id;
            poHeaderEntity._rev = dataEntity._rev;
            poHeaderEntity.type = dataEntity.type;
            poHeaderEntity.group_id = dataEntity.group_id;
            poHeaderEntity.Restaurent_Id = dataEntity.Restaurent_Id;
            poHeaderEntity.PONumber = dataEntity.PONumber;
            poHeaderEntity.PO_date = dataEntity.PO_date;
            poHeaderEntity.Document_Number = dataEntity.Document_Number;
            poHeaderEntity.Supplier_Id = dataEntity.Supplier_Id;
            poHeaderEntity.Status = dataEntity.Status;
            poHeaderEntity.User_Id = dataEntity.User_Id;
            poHeaderEntity.Remarks = dataEntity.Remarks;
            poHeaderEntity.PaymentTermID = dataEntity.PaymentTermID;
            poHeaderEntity.NetPrice = dataEntity.NetPrice;
            poHeaderEntity.TotalPrice = dataEntity.TotalPrice;
            poHeaderEntity.FullDiscountRate = dataEntity.FullDiscountRate;
            poHeaderEntity.DiscountPrice = dataEntity.DiscountPrice;
            poHeaderEntity.LineDiscountPrice = dataEntity.LineDiscountPrice;
            poHeaderEntity.VATPrice = dataEntity.VATPrice;
            poHeaderEntity.OtherTaxPrice = dataEntity.OtherTaxPrice;
            poHeaderEntity.AdvanceAmount = dataEntity.AdvanceAmount;
            poHeaderEntity.DiscountAmount = dataEntity.DiscountAmount;

            if (dataEntity.Items != undefined) {
                var detailMapper = new PODetaiEntityMapper();
                dataEntity.Items.forEach(function (item) {
                    var poItemEntity = new detailMapper.map(item);
                    POHeaderEntity.Items.add(poItemEntity);
                });
            }
            return poHeaderEntity;
        } catch (error) {
            throw error;
        }

    };

    mapForDelete(dataEntity) {
        try {
            var poHeaderEntity = new POHeaderEntity();
            poHeaderEntity._id = dataEntity._id;
            poHeaderEntity._rev = dataEntity._rev;
            poHeaderEntity.type = dataEntity.type;
            poHeaderEntity.group_id = dataEntity.group_id;
            poHeaderEntity.Restaurent_Id = dataEntity.Restaurent_Id;
            poHeaderEntity.PONumber = dataEntity.PONumber;
            poHeaderEntity.PO_date = dataEntity.PO_date;
            poHeaderEntity.Document_Number = dataEntity.Document_Number;
            poHeaderEntity.Supplier_Id = dataEntity.Supplier_Id;
            poHeaderEntity.Status = StatusConstants.DELETED;
            poHeaderEntity.User_Id = dataEntity.User_Id;
            poHeaderEntity.Remarks = dataEntity.Remarks;
            poHeaderEntity.PaymentTermID = dataEntity.PaymentTermID;
            poHeaderEntity.NetPrice = dataEntity.NetPrice;
            poHeaderEntity.TotalPrice = dataEntity.TotalPrice;
            poHeaderEntity.FullDiscountRate = dataEntity.FullDiscountRate;
            poHeaderEntity.DiscountPrice = dataEntity.DiscountPrice;
            poHeaderEntity.LineDiscountPrice = dataEntity.LineDiscountPrice;
            poHeaderEntity.VATPrice = dataEntity.VATPrice;
            poHeaderEntity.OtherTaxPrice = dataEntity.OtherTaxPrice;
            poHeaderEntity.AdvanceAmount = dataEntity.AdvanceAmount;
            poHeaderEntity.DiscountAmount = dataEntity.DiscountAmount;

            if (dataEntity.Items != undefined) {
                var detailMapper = new PODetaiEntityMapper();
                dataEntity.Items.forEach(function (item) {
                    var poItemEntity = new detailMapper.mapForDelete(item);
                    POHeaderEntity.Items.add(poItemEntity);
                });
            }
            return poHeaderEntity;
        } catch (error) {
            throw error;
        }

    };

    mapForFinalApproval(dataEntity) {
        try {
            var poHeaderEntity = new POHeaderEntity();
            poHeaderEntity._id = dataEntity._id;
            poHeaderEntity._rev = dataEntity._rev;
            poHeaderEntity.type = dataEntity.type;
            poHeaderEntity.group_id = dataEntity.group_id;
            poHeaderEntity.Restaurent_Id = dataEntity.Restaurent_Id;
            poHeaderEntity.PONumber = dataEntity.PONumber;
            poHeaderEntity.PO_date = dataEntity.PO_date;
            poHeaderEntity.Document_Number = dataEntity.Document_Number;
            poHeaderEntity.Supplier_Id = dataEntity.Supplier_Id;
            poHeaderEntity.Status = StatusConstants.APPROVED;
            poHeaderEntity.User_Id = dataEntity.User_Id;
            poHeaderEntity.Remarks = dataEntity.Remarks;
            poHeaderEntity.PaymentTermID = dataEntity.PaymentTermID;
            poHeaderEntity.NetPrice = dataEntity.NetPrice;
            poHeaderEntity.TotalPrice = dataEntity.TotalPrice;
            poHeaderEntity.FullDiscountRate = dataEntity.FullDiscountRate;
            poHeaderEntity.DiscountPrice = dataEntity.DiscountPrice;
            poHeaderEntity.LineDiscountPrice = dataEntity.LineDiscountPrice;
            poHeaderEntity.VATPrice = dataEntity.VATPrice;
            poHeaderEntity.OtherTaxPrice = dataEntity.OtherTaxPrice;
            poHeaderEntity.AdvanceAmount = dataEntity.AdvanceAmount;
            poHeaderEntity.DiscountAmount = dataEntity.DiscountAmount;

            if (dataEntity.Items != undefined) {
                var detailMapper = new PODetaiEntityMapper();
                dataEntity.Items.forEach(function (item) {
                    var poItemEntity = new detailMapper.mapForFinalApproval(item);
                    POHeaderEntity.Items.add(poItemEntity);
                });
            }
            return poHeaderEntity;
        } catch (error) {
            throw error;
        }

    };
}

module.exports = PurchaseOrderHeaderEntityMapper;
