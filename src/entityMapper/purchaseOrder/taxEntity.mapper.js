
var POTaxEntity = require("../../entities/purchaseOrder/tax.entity");
const UIDGenerator = require("../../../helpers/uniqueIDGenerator.hlpr");

class PurchaseOrderTaxEntityMapper {
    constructor() {
    }

    map(dataDTO) {

        var poTaxEntity = new POTaxEntity();
        if (dataDTO.Line_id == "") {
            dataDTO.Line_id = UIDGenerator.create();
        }
        poTaxEntity.Line_id =dataDTO.Line_id;
        poTaxEntity.Tax_id =dataDTO.Tax_id;
        poTaxEntity.Amount = dataDTO.Amount;

        return poTaxEntity;
    }
}

module.exports = PurchaseOrderTaxEntityMapper
